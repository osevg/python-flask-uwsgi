# Flask Sample Application

This repository provides a sample Python web application implemented using the Flask web framework and hosted using ``uWSGI``. It is intended to be used to demonstrate deployment of Python web applications to OpenShift 3.

## Implementation Notes

This sample Python application relies on the support provided by the default S2I builder for deploying an arbitrary Python web application by including an ``app.py`` file. From this ``app.py`` file a shell script ``app.sh`` is executed and then ``uWSGI`` is launched to host the WSGI application found in the ``wsgi.py`` file. The requirements which need to be satisfied for this to work are:

* The WSGI application code file needs to be named ``wsgi.py``.
* The WSGI application entry point within the code file needs to be named ``application``.
* The ``uWSGI`` package must be listed in the ``requirements.txt`` file for ``pip``.
* The shell script ``app.sh`` needs to be launched from ``app.py``.
* The WSGI server program ``uwsgi`` needs to be launched from ``app.sh``.

An intermediary shell script ``app.sh`` is used so that the ``uwsgi`` program can be found automatically by searching ``PATH``, rather than having to hard code or determine its location so that it can be launched from the ``app.py`` file.


## Deployment Steps

To deploy this sample Python web application from the OpenShift web console, you should select ``python:2.7``, ``python:3.3``, ``python:3.4`` or ``python:latest``, when using _Add to project_. Use of ``python:latest`` is the same as having selected the most up to date Python version available, which at this time is ``python:3.4``.

The HTTPS URL of this code repository which should be supplied to the _Git Repository URL_ field when using _Add to project_ is:

* https://gitlab.com/osevg/python-flask-uwsgi.git

If using the ``oc`` command line tool instead of the OpenShift web console, to deploy this sample Python web application, you can run:

```
oc new-app https://gitlab.com/osevg/python-flask-uwsgi.git
```

In this case, because no language type was specified, OpenShift will determine the language by inspecting the code repository. Because the code repository contains a ``requirements.txt``, it will subsequently be interpreted as including a Python application. When such automatic detection is used, ``python:latest`` will be used.

If needing to select a specific Python version when using ``oc new-app``, you should instead use the form:

```
oc new-app python:2.7~https://gitlab.com/osevg/python-flask-uwsgi.git
```
